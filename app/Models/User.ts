import { DateTime } from "luxon";
import { BaseModel, beforeSave, column } from "@ioc:Adonis/Lucid/Orm";
import Hash from "@ioc:Adonis/Core/Hash";

export default class User extends BaseModel {
  @column({ columnName: "id", isPrimary: true })
  public id: number;

  @column({ columnName: "first_name" })
  public firstname: string;

  @column({ columnName: "last_name" })
  public lastname: string;

  @column({ columnName: "email" })
  public email: string;

  @column({ columnName: "password" })
  public password: string;

  @column.dateTime({ columnName: "created_at", autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({
    columnName: "updated_at",
    autoCreate: true,
    autoUpdate: true,
  })
  public updatedAt: DateTime;

  @beforeSave()
  public static async hashPassword(user: User) {
    user.password = await Hash.make(user.password);
  }
}
