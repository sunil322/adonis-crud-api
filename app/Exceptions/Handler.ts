/*
|--------------------------------------------------------------------------
| Http Exception Handler
|--------------------------------------------------------------------------
|
| AdonisJs will forward all exceptions occurred during an HTTP request to
| the following class. You can learn more about exception handling by
| reading docs.
|
| The exception handler extends a base `HttpExceptionHandler` which is not
| mandatory, however it can do lot of heavy lifting to handle the errors
| properly.
|
*/

import Logger from "@ioc:Adonis/Core/Logger";
import HttpExceptionHandler from "@ioc:Adonis/Core/HttpExceptionHandler";

export default class ExceptionHandler extends HttpExceptionHandler {
  constructor() {
    super(Logger);
  }

  public async handle(error: any, { response, params }) {
    if (error.code === "E_ROW_NOT_FOUND") {
      const { id } = params;
      response.status(404).json({
        message: `User not found with id: ${id}`,
        timestamp: new Date(),
      });
    } else if (error.code === "E_ROUTE_NOT_FOUND") {
      response.status(404).json({
        message: "Api path doesn't exist",
        timestamp: new Date(),
      });
    } else if(error.code === 'E_VALIDATION_FAILURE'){
      const allValidationMessages = error.messages.errors.reduce((allValidationErrors:Object[], error:{field:string,message:string}) => {
        const validationMessage={};
        validationMessage['field'] = error.field,
        validationMessage['message'] = error.message;
        allValidationErrors.push(validationMessage);
        return allValidationErrors;
      },[])
        
      response.status(400).json({
        message: allValidationMessages,
        timestamp: new Date(),
      });
    }else if (error.message === "ER_DUP_ENTRY") {
      response.status(409).json({
        message: "Email already exists",
        timestamp: new Date(),
      });
    } else if(error.code === 'FIELD_UNDEFINED'){
      return response.status(400).json({
        message: [{
          field: `${error.field}`,
          message: `${error.field} is required`,
        }],
        timestamp: new Date(),
      })
    }else {
      response.status(500).json({
        message: "Something went wrong",
        timestamp: new Date(),
      });
    }
  }
}
