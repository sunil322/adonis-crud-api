import CreateUserValidator from "App/Validators/CreateUserValidator";
import User from "App/Models/User";

export default class UsersController {

  public async createUser({ request, response }) {
    try {
      const { firstname, lastname, email, password } = await request.validate(
        CreateUserValidator
      );

      if (firstname === undefined) {
        throw {code:'FIELD_UNDEFINED',field:'firstname'}; 
      } else if (lastname === undefined) {
        throw {code:'FIELD_UNDEFINED',field:'lastname'}; 
      } else if (email === undefined) {
        throw {code:'FIELD_UNDEFINED',field:'email'}; 
      } else if (password === undefined) {
        throw {code:'FIELD_UNDEFINED',field:'password'}; 
      } else {
        const savedUser = await User.create({
          firstname,
          lastname,
          email: email.toLowerCase(),
          password,
        });
        response.status(201).json(savedUser);
      }
    } catch (error) {
      if (error.constraint === "users_email_unique") {
        throw new Error("ER_DUP_ENTRY");
      } else {
        throw error;
      }
    }
  }

  public async getUsers({ response }) {
    const users = await User.all();
    response.status(200).json(users);
  }

  public async getUserById({ response, params }) {
    const { id } = params;
    const user = await User.findOrFail(id);
    response.status(200).json(user);
  }

  public async updateUser({ request, response, params }) {
    const { id } = params;
    try {
      const { firstname, lastname, email, password } = await request.validate(
        CreateUserValidator
      );

      if (firstname === undefined) {
        throw {code:'FIELD_UNDEFINED',field:'firstname'};
      } else if (lastname === undefined) {
        throw {code:'FIELD_UNDEFINED',field:'lastname'};
      } else if (email === undefined) {
        throw {code:'FIELD_UNDEFINED',field:'email'};
      } else if (password === undefined) {
        throw {code:'FIELD_UNDEFINED',field:'password'};
      } else {
        const existedUser = await User.findOrFail(id);
        existedUser.firstname = firstname;
        existedUser.lastname = lastname;
        existedUser.email = email;
        existedUser.password = password;
        const updatedUser = await existedUser.save();
        response.status(200).json(updatedUser);
      }
    } catch (error) {
      if (error.constraint === "users_email_unique") {
        throw new Error("ER_DUP_ENTRY");
      } else {
        throw error;
      }
    }
  }

  public async deleteUser({ response, params }) {
    const { id } = params;
    const user = await User.findOrFail(id);
    await user.delete();
    response.status(200).json({
      message: `User with id: ${id} deleted successfully`,
      timestamp: new Date(),
    });
  }

  public getApiDetails({response}){
    response.status(200).json({
      'create user': 'POST /users/',
      'get all users':'GET /users/',
      'get user by id': 'GET /users/:id',
      'update user by id': 'PUT /users/:id',
      'delete user by id': 'DELETE /users/:id'
    })
  }
}
