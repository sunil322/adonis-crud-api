import Route from "@ioc:Adonis/Core/Route";

Route.get('/',"UsersController.getApiDetails").as("get_api_details");

Route.post("/users", "UsersController.createUser").as("create_user");

Route.get("/users", "UsersController.getUsers").as("get_users");

Route.get("/users/:id", "UsersController.getUserById")
  .where("id", {
    match: /^[0-9]+$/,
    cast: (id) => {
      return Number(id);
    },
  })
  .as("get_user_by_id");

Route.put("/users/:id", "UsersController.updateUser")
  .where("id", {
    match: /^[0-9]+$/,
    cast: (id) => {
      return Number(id);
    },
  })
  .as("update_user");

Route.delete("/users/:id", "UsersController.deleteUser")
  .where("id", {
    match: /^[0-9]+$/,
    cast: (id) => {
      return Number(id);
    },
  })
  .as("delete_user");